import * as React from 'react';
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Icon,
  Text,
  View,
  Form,
  Picker,
  Row
} from 'native-base';

const App = () => {
  // Guy score
  const [solitaryStone, setSolitaryStone] = React.useState<boolean>(true);
  const [multipleStone, setMultipleStone] = React.useState<boolean>(true);
  const [hydronephrosis, setHydronephrosis] = React.useState<boolean>(true);
  const [partialStaghorn, setPartialStaghorn] = React.useState<boolean>(true);
  const [staghorn, setStaghorn] = React.useState<boolean>(true);
  const [diverticuli, setDiverticuli] = React.useState<boolean>(true);
  const [pole, setPole] = React.useState<string>('Low');
  const [guyTotalScore, setGuyTotalScore] = React.useState<number>(1);
  const [guyPercent, setGuyPercent] = React.useState<number>(0);

  // Stone score
  const [stoneSize, setStoneSize] = React.useState<string>('0 - 399');
  const [tractLenght, setTractLenght] = React.useState<string>('< 100');
  const [stoneHydronephrosis, setStoneHydronephrosis] = React.useState<string>('NO/MILD');
  const [calyces, setCalyces] = React.useState<string>('1 - 2');
  const [density, setDensity] = React.useState<string>('< 950');
  const [stoneTotalScore, setStoneTotalScore] = React.useState<number>(5);
  const [stonePercent, setStonePercent] = React.useState<number>(0);


  // croes score
  const [stoneBurden, setStoneBurden] = React.useState<string>('1200');
  const [stoneLocation, setStoneLocation] = React.useState<string>('MULTIPLE');
  const [croesStaghorn, setCroesStaghorn] = React.useState<boolean>(true);
  const [caseVY, setCaseVY] = React.useState<number>(0);
  const [priorTreatment, setPriorTreatment] = React.useState<string>('None');
  const [numberStones, setNumberStones] = React.useState<string>('Single');
  const [croesTotalScore, setCroesTotalScore] = React.useState<number>(5);
  const [croesPercent, setCroesPercent] = React.useState<number>(0);


  function handleChange(value: any, con: any) {
    con(value);
  }

  React.useEffect(
    () => {
      calculateGuyScore();
    }, [
    solitaryStone,
    pole,
    multipleStone,
    hydronephrosis,
    partialStaghorn,
    staghorn,
    diverticuli,
  ]
  )

  React.useEffect(
    () => {
      calculateStoneScore();
    }, [
    stoneSize,
    tractLenght,
    stoneHydronephrosis,
    calyces,
    density,
  ]
  )

  React.useEffect(
    () => {
      calculateCroesScore();
    }, [
    stoneBurden,
    stoneLocation,
    croesStaghorn,
    caseVY,
    priorTreatment,
    numberStones,
  ]
  )

  React.useEffect(
    () => {
      let auxPercent = guyTotalScore === 4 ? 29 :
        guyTotalScore === 3 ? 35 :
          guyTotalScore === 2 ? 72.4 : 81;

      setGuyPercent(auxPercent)

    }, [guyTotalScore]
  )

  React.useEffect(
    () => {
      setStonePercent(stoneTotalScore === 5 ? 94 :
        stoneTotalScore === 6 ? 88 :
          stoneTotalScore === 7 ? 92 :
            stoneTotalScore === 8 ? 83 :
              stoneTotalScore === 9 ? 64 :
                stoneTotalScore === 10 ? 42 : 27)

    }, [stoneTotalScore]
  )

  React.useEffect(
    () => {
      setCroesPercent(croesTotalScore < 65 ? 45 :
        (croesTotalScore > 65 && croesTotalScore <= 81) ? 50 :
          (croesTotalScore > 81 && croesTotalScore <= 91) ? 55 :
            (croesTotalScore > 91 && croesTotalScore <= 106) ? 60 :
              (croesTotalScore > 106 && croesTotalScore <= 121) ? 65 :
                (croesTotalScore > 121 && croesTotalScore <= 131) ? 70 :
                  (croesTotalScore > 131 && croesTotalScore <= 151) ? 75 :
                    (croesTotalScore > 151 && croesTotalScore <= 171) ? 80 :
                      (croesTotalScore > 171 && croesTotalScore <= 191) ? 85 :
                        (croesTotalScore > 191 && croesTotalScore <= 221) ? 90 : 95)

    }, [croesTotalScore]
  )

  function calculateGuyScore() {
    if (staghorn) {
      setGuyTotalScore(4)
      return
    }

    if (diverticuli || partialStaghorn || hydronephrosis) {
      setGuyTotalScore(3)
      return
    }

    if (pole === 'Upper' || pole === 'Multiple') {
      setGuyTotalScore(2)
      return
    }

    setGuyTotalScore(1);
  }

  function calculateStoneScore() {
    let sizeAux = stoneSize === '0 - 399' ? 1 :
      stoneSize === '400 - 799' ? 2 :
        stoneSize === '800 - 1599' ? 3 : 4;

    let tractAux = tractLenght === '< 100' ? 1 : 2;
    let hydroAux = stoneHydronephrosis === 'NO/MILD' ? 1 : 2;
    let calycesAux = calyces === '1 - 2' ? 1 :
      calyces === '3' ? 2 : 3;
    let densityAux = density === '> 950' ? 1 : 2;

    setStoneTotalScore(sizeAux + tractAux + hydroAux + calycesAux + densityAux);
  }

  function calculateCroesScore() {
    let burdenAux = stoneBurden === '1200' ? 0 :
      stoneBurden === '1199 - 1150' ? 2.5 :
        stoneBurden === '1149 - 1101' ? 5 :
          stoneBurden === '1100' ? 7.5 :
            stoneBurden === '1099 - 1001' ? 10 :
              stoneBurden === '1000' ? 12.5 :
                stoneBurden === '999 - 950' ? 15 :
                  stoneBurden === '949 - 901' ? 17.5 :
                    stoneBurden === '900' ? 20 :
                      stoneBurden === '899 - 850' ? 22.5 :
                        stoneBurden === '849 - 801' ? 25 :
                          stoneBurden === '800' ? 27.5 :
                            stoneBurden === '799 - 750' ? 30 :
                              stoneBurden === '749 - 701' ? 31 :
                                stoneBurden === '700' ? 32.5 :
                                  stoneBurden === '699 - 650' ? 35 :
                                    stoneBurden === '649 - 601' ? 37.5 :
                                      stoneBurden === '600' ? 39 :
                                        stoneBurden === '599 - 550' ? 42.5 :
                                          stoneBurden === '549 - 501' ? 43.75 :
                                            stoneBurden === '500' ? 46 :
                                              stoneBurden === '499 - 450' ? 47.5 :
                                                stoneBurden === '449 - 401' ? 50 :
                                                  stoneBurden === '400' ? 52.5 :
                                                    stoneBurden === '399 - 350' ? 55 :
                                                      stoneBurden === '349 - 301' ? 57 :
                                                        stoneBurden === '300' ? 59 :
                                                          stoneBurden === '299 - 250' ? 61.5 :
                                                            stoneBurden === '249 - 201' ? 62.5 :
                                                              stoneBurden === '200' ? 65 :
                                                                stoneBurden === '199 - 150' ? 67.5 :
                                                                  stoneBurden === '149 - 101' ? 70 :
                                                                    stoneBurden === '100' ? 72.5 : 75;
    let locationAux = stoneLocation === 'MULTIPLE' ? 0 :
      stoneLocation === 'UPPER' ? 11 :
        stoneLocation === 'LOWER' ? 45 :
          stoneLocation === 'PELVIC' ? 57.5 : 70;
    let staghornAux = croesStaghorn ? 0 : 21;
    let caseAux = caseVY === 0 ? 0 :
      caseVY === 10 ? 10.625 :
        caseVY === 20 ? 21.25 :
          caseVY === 30 ? 31.8 :
            caseVY === 40 ? 42.5 :
              caseVY === 50 ? 51.75 :
                caseVY === 60 ? 61 :
                  caseVY === 70 ? 70 :
                    caseVY === 80 ? 79 :
                      caseVY === 90 ? 84 :
                        caseVY === 100 ? 89 :
                          caseVY === 110 ? 93.25 : 97.5;

    let priorAux = priorTreatment === 'Multiple modalities' ? 3.75 :
      priorTreatment === 'Pyelolithotomy' ? 0 :
        priorTreatment === 'PCNL' ? 17.5 :
          priorTreatment === 'Ureteroscopic' ? 19 :
            priorTreatment === 'SWL' ? 20 : 35;

    let numberStonesAux = numberStones === 'Single' ? 35 : 0;

    setCroesTotalScore(burdenAux + priorAux + numberStonesAux + locationAux + staghornAux + caseAux);
  }

  return (
    <>
      <Header>
        <Body>
          <Title>Medical calculator</Title>
        </Body>
        <Right />
      </Header>
      <Content style={{ alignContent: 'center', flex: 1 }} >
        <View style={{ flex: 1, padding: 15 }}>
          <Text style={{ fontSize: 20, fontWeight: 'bold' }}>
            Guy score
          </Text>
          <Form style={{ marginTop: 5 }}>
            <Text>
              Solitary stone
            </Text>
            <Picker
              note
              mode="dropdown"
              selectedValue={solitaryStone}
              onValueChange={(value) => handleChange(value, setSolitaryStone)}
            >
              <Picker.Item label="Yes" value={true} />
              <Picker.Item label="No" value={false} />
            </Picker>
            <Text>
              Wich pole?
            </Text>
            <Picker
              note
              mode="dropdown"
              selectedValue={pole}
              onValueChange={(value) => handleChange(value, setPole)}
            >
              <Picker.Item label="Low" value='Low' />
              <Picker.Item label="Mid" value='Mid' />
              <Picker.Item label="Upper" value='Upper' />
              <Picker.Item label="Pelvic" value='Pelvic' />
              <Picker.Item label="Multiple" value='Multiple' />
            </Picker>
            <Text>
              Multiple stone
            </Text>
            <Picker
              note
              mode="dropdown"
              selectedValue={multipleStone}
              onValueChange={(value) => handleChange(value, setMultipleStone)}
            >
              <Picker.Item label="Yes" value={1} />
              <Picker.Item label="No" value={0} />
            </Picker>
            <Text>
              Hydronephrosis
            </Text>
            <Picker
              note
              mode="dropdown"
              selectedValue={hydronephrosis}
              onValueChange={(value) => handleChange(value, setHydronephrosis)}
            >
              <Picker.Item label="Yes" value={1} />
              <Picker.Item label="No" value={0} />
            </Picker>
            <Text>
              Partial Staghorn
            </Text>
            <Picker
              note
              mode="dropdown"
              selectedValue={partialStaghorn}
              onValueChange={(value) => handleChange(value, setPartialStaghorn)}
            >
              <Picker.Item label="Yes" value={1} />
              <Picker.Item label="No" value={0} />
            </Picker>
            <Text>
              Staghorn
            </Text>
            <Picker
              note
              mode="dropdown"
              selectedValue={staghorn}
              onValueChange={(value) => handleChange(value, setStaghorn)}
            >
              <Picker.Item label="Yes" value={1} />
              <Picker.Item label="No" value={0} />
            </Picker>

            <Text>
              Stone in Diverticuli?
            </Text>
            <Picker
              note
              mode="dropdown"
              selectedValue={diverticuli}
              onValueChange={(value) => handleChange(value, setDiverticuli)}
            >
              <Picker.Item label="Yes" value={1} />
              <Picker.Item label="No" value={0} />
            </Picker>
            <Text>
              {`Total Score: ${guyTotalScore}`}
            </Text>

            <Text>
              {guyPercent}%
            </Text>
          </Form>
        </View>

        {/* stone score */}
        <View style={{ flex: 1, padding: 15 }}>
          <Text style={{ fontSize: 20, fontWeight: 'bold' }}>
            Stone score
          </Text>
          <Form style={{ marginTop: 5 }}>
            <Text>
              Stone size (mm2)
            </Text>
            <Picker
              note
              mode="dropdown"
              selectedValue={stoneSize}
              onValueChange={(value) => handleChange(value, setStoneSize)}
            >
              <Picker.Item label="0 - 399" value={'0 - 399'} />
              <Picker.Item label="400 - 799" value={'400 - 799'} />
              <Picker.Item label="800 - 1599" value={'800 - 1599'} />
              <Picker.Item label="> 1600" value={'> 1600'} />
            </Picker>
            <Text>
              Tract lenght (mm)
            </Text>
            <Picker
              note
              mode="dropdown"
              selectedValue={tractLenght}
              onValueChange={(value) => handleChange(value, setTractLenght)}
            >
              <Picker.Item label="< 100" value='Low' />
              <Picker.Item label="> 100" value='Mid' />
            </Picker>
            <Text>
              Hydronephrosis
            </Text>
            <Picker
              note
              mode="dropdown"
              selectedValue={stoneHydronephrosis}
              onValueChange={(value) => handleChange(value, setStoneHydronephrosis)}
            >
              <Picker.Item label="NO/MILD" value={'NO/MILD'} />
              <Picker.Item label="Moderate" value={'Moderate'} />
              <Picker.Item label="Severe" value={'Severe'} />
            </Picker>
            <Text>
              Calyces with stone
            </Text>
            <Picker
              note
              mode="dropdown"
              selectedValue={calyces}
              onValueChange={(value) => handleChange(value, setCalyces)}
            >
              <Picker.Item label="1 - 2" value={'1 - 2'} />
              <Picker.Item label="3" value={'3'} />
              <Picker.Item label="Staghorn" value={'Staghorn'} />
            </Picker>
            <Text>
              Density(HU)
            </Text>
            <Picker
              note
              mode="dropdown"
              selectedValue={density}
              onValueChange={(value) => handleChange(value, setDensity)}
            >
              <Picker.Item label="< 950" value={'< 950'} />
              <Picker.Item label="> 950" value={'> 950'} />
            </Picker>

            <Text>
              {`Total Score: ${stoneTotalScore}`}
            </Text>

            <Text>
              {stonePercent}%
            </Text>
          </Form>
        </View>

        {/* croes score */}
        <View style={{ flex: 1, padding: 15 }}>
          <Text style={{ fontSize: 20, fontWeight: 'bold' }}>
            Croes score
          </Text>
          <Form style={{ marginTop: 5 }}>
            <Text>
              Stone burden
            </Text>
            <Picker
              note
              mode="dropdown"
              selectedValue={stoneBurden}
              onValueChange={(value) => handleChange(value, setStoneBurden)}
            >
              <Picker.Item label="1200" value={'1200'} />
              <Picker.Item label="1199 - 1150" value={'1199 - 1950'} />
              <Picker.Item label="1149 - 1101" value={'1149 - 1101'} />
              <Picker.Item label="1100" value={'1100'} />
              <Picker.Item label="1099 - 1001" value={'1099 - 1001'} />
              <Picker.Item label="1000" value={'1000'} />
              <Picker.Item label="999 - 950" value={'999 - 950'} />
              <Picker.Item label="949 - 901" value={'949 - 901'} />
              <Picker.Item label="900" value={'900'} />
              <Picker.Item label="899 - 850" value={'899 - 850'} />
              <Picker.Item label="849 - 801" value={'849 - 801'} />
              <Picker.Item label="800" value={'800'} />
              <Picker.Item label="799 - 750" value={'799 - 750'} />
              <Picker.Item label="749 - 701" value={'749 - 701'} />
              <Picker.Item label="700" value={'700'} />
              <Picker.Item label="699 - 650" value={'699 - 650'} />
              <Picker.Item label="649 - 601" value={'649 - 601'} />
              <Picker.Item label="600" value={'600'} />
              <Picker.Item label="599 - 550" value={'599 - 550'} />
              <Picker.Item label="549 - 501" value={'549 - 501'} />
              <Picker.Item label="500" value={'500'} />
              <Picker.Item label="499 - 450" value={'499 - 450'} />
              <Picker.Item label="449 - 401" value={'449 - 401'} />
              <Picker.Item label="400" value={'400'} />
              <Picker.Item label="399 - 350" value={'399 - 350'} />
              <Picker.Item label="349 - 301" value={'349 - 301'} />
              <Picker.Item label="300" value={'300'} />
              <Picker.Item label="299 - 250" value={'299 - 250'} />
              <Picker.Item label="249 - 201" value={'249 - 201'} />
              <Picker.Item label="200" value={'200'} />
              <Picker.Item label="199 - 150" value={'199 - 150'} />
              <Picker.Item label="149 - 101" value={'149 - 101'} />
              <Picker.Item label="100" value={'100'} />
              <Picker.Item label="< 100" value={'< 100'} />

            </Picker>
            <Text>
              Stone location
            </Text>
            <Picker
              note
              mode="dropdown"
              selectedValue={stoneLocation}
              onValueChange={(value) => handleChange(value, setStoneLocation)}
            >
              <Picker.Item label="MULTIPLE" value='MULTIPLE' />
              <Picker.Item label="UPPER" value='UPPER' />
              <Picker.Item label="LOWER" value='LOWER' />
              <Picker.Item label="PELVIC" value='PELVIC' />
              <Picker.Item label="MID" value='MID' />
            </Picker>
            <Text>
              Staghorn
            </Text>
            <Picker
              note
              mode="dropdown"
              selectedValue={croesStaghorn}
              onValueChange={(value) => handleChange(value, setCroesStaghorn)}
            >
              <Picker.Item label="Yes" value={true} />
              <Picker.Item label="No" value={false} />
            </Picker>
            <Text>
              Case / Volumen / Year
            </Text>
            <Picker
              note
              mode="dropdown"
              selectedValue={caseVY}
              onValueChange={(value) => handleChange(value, setCaseVY)}
            >
              <Picker.Item label='0' value={0} />
              <Picker.Item label='10' value={10} />
              <Picker.Item label='20' value={20} />
              <Picker.Item label='30' value={30} />
              <Picker.Item label='40' value={40} />
              <Picker.Item label='50' value={50} />
              <Picker.Item label='60' value={60} />
              <Picker.Item label='70' value={70} />
              <Picker.Item label='80' value={80} />
              <Picker.Item label='90' value={90} />
              <Picker.Item label='100' value={100} />
              <Picker.Item label='110' value={110} />
              <Picker.Item label='120' value={120} />
            </Picker>
            <Text>
              Prior Treatment
            </Text>
            <Picker
              note
              mode="dropdown"
              selectedValue={priorTreatment}
              onValueChange={(value) => handleChange(value, setPriorTreatment)}
            >
              <Picker.Item label='None' value={'None'} />
              <Picker.Item label='Pyelolithotomy' value={'Pyelolithotomy'} />
              <Picker.Item label='Multiple modalities' value={'Multiple modalities'} />
              <Picker.Item label='PCNL' value={'PCNL'} />
              <Picker.Item label='Ureteroscopic' value={'Ureteroscopic'} />
              <Picker.Item label='SWL' value={'SWL'} />

            </Picker>

            <Text>
              Number of stones
            </Text>
            <Picker
              note
              mode="dropdown"
              selectedValue={numberStones}
              onValueChange={(value) => handleChange(value, setNumberStones)}
            >
              <Picker.Item label='Single' value={'Single'} />
              <Picker.Item label='Multiple' value={'Multiple'} />

            </Picker>

            <Text>
              {`Total Score: ${croesTotalScore}`}
            </Text>

            <Text>
              {croesPercent}%
            </Text>

            <Text style={{ fontSize: 20, fontWeight: 'bold', marginTop: 10 }}>
              Calculus score: {(guyPercent + stonePercent + croesPercent) / 3}
            </Text>
          </Form>
        </View>
      </Content>
    </>
  )
}
export default App;
